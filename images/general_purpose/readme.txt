The images in this folder can be used in Lazarus applications as toolbar or
button icons. They come in several sizes as required by the LCL scaling for
high-dpi screens: 

- 16x16, 24x24 und 32x32 pixels for "small" images, and
- 24x24, 36x36 and 48x48 for "medium" sized images, and
- 32x32, 48x48 and 64x64 for "large" images.

The images were kindly provided by Roland Hahn. 

License:
Creative Commons CC0 1.0 Universal
(freely available, no restrictions in usage)